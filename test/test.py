try:
    import json
except ImportError:
    import simplejson as json
import pytest
from M2Crypto import BIO
from M2Crypto import SMIME
from M2Crypto import X509
from M2Crypto import m2
from path import Path

from passbook.models import Barcode, BarcodeFormat, Pass, StoreCard

cwd = Path(__file__).parent

wwdr_certificate = cwd / 'wwdr.pem'
certificate = cwd / 'certificates/membershipVoucherCert.pem'
key = cwd / 'certificates/membershipVoucherCertKEY.pem'
# password_file = cwd / 'certificates/password.txt'

# wwdr_certificate = open('wwdr.pem')
# certificate = open('membershipVoucherCert.pem')
# key = open('membershipVoucherCertKEY.pem')
#password_file = open('password.txt')

cardInfo = StoreCard()
cardInfo.addPrimaryField('name', 'John Doe', 'Name')

organizationName = 'Your organization' 
passTypeIdentifier = 'pass.com.your.organization' 
teamIdentifier = 'AGK5BZEN3E'

passfile = Pass(cardInfo, \
    passTypeIdentifier=passTypeIdentifier, \
    organizationName=organizationName, \
    teamIdentifier=teamIdentifier)
passfile.serialNumber = '1234567' 
passfile.barcode = Barcode(message = 'Barcode message')    
# Including the icon and logo is necessary for the passbook to be valid.
passfile.addFile('icon.png', open('static/white_square.png', 'r'))
passfile.addFile('logo.png', open('static/white_square.png', 'r'))
# Create and output the Passbook file (.pkpass) 
# wwdr_certificate = wwdr_certificate.read()
pkpass = passfile.create(certificate, key, wwdr_certificate, '123456')
file = open(pkpass)